# README #


### SDGANの実装
dcgan.py

DCGANをKerasを使って実装した。

### ニューラルネットワークの実装
neural_network_test.py

ニューラルネットワークを動かしてみようと思い、実装してみた。
3層のニューラルネットワークで、入力層、2つの中間層、出力層からなるニューラルネットワークである。

なお、このコードは、『ゼロから作る Deep Learning』という書籍にある2層のニューラルネットワークのコードを参考にした。
https://github.com/oreilly-japan/deep-learning-from-scratch


### その他の参考文献
岡谷貴之. （2015）　*深層学習* (機械学習プロフェッショナルシリーズ) 講談社.